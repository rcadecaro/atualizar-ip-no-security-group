    # ATUALIZAR IP NO SECURITY GROUP

Este projeto tem o intuito de você agendar em uma cron em uma maquina na sua rede local para rodar de tempos em tempos e o script irá pegar o seu IP público e irá atualizar nos security groups definidos no script.

# Como usar

  - Edite o arquivo do script e edite a variável da descrição para IPV4 e IPV6 assim colocando uma descrição unica para sua liberação.
  - No linux: Coloque o script para rodar no cron utilizando-o da seguinte forma: 
 ```sh
$ /path/to/script/update-my-ip-for-linux.sh [SECURITY-GROUP-ID] [PORT] [REGION]
```
  - No Windows: Você irá precisar editar os arquivos e editar as variáveis de porta, protocolo, descrição e id do grupo de segurança, após isto coloque o script para rodar no agendador de tarefas.